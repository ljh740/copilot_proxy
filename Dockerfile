# Stage 1: Build the Dart application
FROM dart:3.4.0 as build

# Set working directory
WORKDIR /build

# Copy the application source code
COPY . .

# Get Dart dependencies
RUN dart pub get

# Compile the Dart application
RUN dart compile exe bin/copilot_proxy.dart -o ./copilot_proxy_linux

# Stage 2: Create the runtime image
FROM dart:3.4.0

# Create directory for the application
RUN mkdir /app

# Copy the compiled Dart application from the build stage
COPY --from=build /build/copilot_proxy_linux /app/copilot_proxy_linux

# Set the working directory for the application
WORKDIR /config

# Define the entrypoint
ENTRYPOINT ["/app/copilot_proxy_linux"]
