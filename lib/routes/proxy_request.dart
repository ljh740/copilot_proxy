import 'dart:convert';
import 'dart:io';

import 'package:copilot_proxy/common.dart';
import 'package:copilot_proxy/context.dart';
import 'package:copilot_proxy/header_manager.dart';
import 'package:copilot_proxy/routes/routes.dart';
import 'package:copilot_proxy/token_manager.dart';
import 'package:copilot_proxy/utils/json_util.dart';
import 'package:copilot_proxy/utils/network_util.dart';

RestHandler proxyRequest(RequestType type) {
  return (Context context) {
    final String username = context['username'];
    switch (type) {
      case RequestType.codex:
        return TokenManager.instance.useCodexTokenData(
          username,
          (token) => _request(
            context,
            token,
            token?.codexUrl,
            type,
          ),
        );
      case RequestType.chat:
        return TokenManager.instance.useChatTokenData(
          username,
          (token) => _request(
            context,
            token,
            token?.chatUrl,
            type,
          ),
        );
    }
  };
}

Future<void> _request(Context context, TokenData? token, Uri? uri, RequestType type) async {
  if (context.isTokenDataWrong(token)) return;
  if (token!.type == 'copilot') {
    await _copilotRequest(context, token, uri);
  } else if (token.type == 'openai') {
    await _openaiRequest(context, token, uri, type);
  }
}

Future<void> _openaiRequest(Context context, TokenData token, Uri? uri, RequestType type) async {
  final JsonMap body = context['json'];

  final request = await token.client.postUrl(uri!);
  switch (type) {
    case RequestType.codex:
      _codexRequest(body, request, token);
      break;
    case RequestType.chat:
      _chatRequest(body, request, token);
      break;
  }
  final resp = await request.close();
  final statusCode = resp.statusCode;
  context.statusCode = statusCode;
  context.response.headers.contentType = resp.headers.contentType;
  await resp.pipe(context.response);
  if (statusCode == HttpStatus.ok) return;
  token.invalidated();
}

void _chatRequest(JsonMap body, HttpClientRequest request, TokenData token) {
  if (!body.containsKey('function_call')) {
    final JsonList messages = body['messages'];
    final JsonMap message = messages.last;
    if (!message['content'].contains('Respond in the following locale')) {
      message['content'] += 'Respond in the following locale: ${config.local}.';
    }
    body.remove('intent');
    body.remove('intent_threshold');
    body.remove('intent_content');
    final model = body['model'];
    body['model'] = token.modelMap[model] ?? model;
    request.setHeaders({
      HttpHeaders.authorizationHeader: ['Bearer ${token.requestToken}'],
      HttpHeaders.contentTypeHeader: [ContentType.json.toString()],
    });
    request.send(jsonEncode(body));
  }
}

void _codexRequest(JsonMap body, HttpClientRequest request, TokenData token) {
  body.remove('extra');
  body.remove('nwo');
  final model = body['model'];
  body['model'] = token.modelMap[model] ?? model;
  request.setHeaders({
    HttpHeaders.authorizationHeader: ['Bearer ${token.requestToken}'],
    HttpHeaders.contentTypeHeader: [ContentType.json.toString()],
  });
  request.send(jsonEncode(body));
}

Future<void> _copilotRequest(Context context, TokenData token, Uri? uri) async {
  final request = await token.client.postUrl(uri!);
  final headers = HeaderManager.instance.getHeaders(uri.path);
  request.setHeaders({
    ...headers,
    HttpHeaders.authorizationHeader: ['Bearer ${token.requestToken}'],
  });
  request.send(context['body']);
  final resp = await request.close();
  final statusCode = resp.statusCode;
  context.statusCode = statusCode;
  resp.headers.forEach((k, v) => context.response.headers.set(k, v));
  await resp.pipe(context.response);
  if (statusCode == HttpStatus.ok) return;
  token.setExpiry();
}

enum RequestType {
  codex,
  chat,
}
