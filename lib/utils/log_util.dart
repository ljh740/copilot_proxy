import 'dart:io';

final _logFile = File('copilot_proxy.log');

bool noLog = false;

void log(Object? object, [bool printToConsole = true, int length = 100]) {
  if (!noLog) _logFile.writeAsStringSync('$object\n', mode: FileMode.append);
  final str = object.toString();
  if (printToConsole) print(str.length <= length ? str : str.substring(0, length));
}

void logError(Object? object, [StackTrace? s, bool printToConsole = true]) {
  log('error: $object', printToConsole);
  if (s != null) {
    log('stackTrace: $s', printToConsole);
  } else if (object is Error) {
    log('stackTrace: ${object.stackTrace}', printToConsole);
  }
}
